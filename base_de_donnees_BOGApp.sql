-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3307
-- Généré le : dim. 19 juin 2022 à 15:43
-- Version du serveur : 10.6.5-MariaDB
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bog`
--
CREATE DATABASE IF NOT EXISTS `bog` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bog`;

-- --------------------------------------------------------

--
-- Structure de la table `brand`
--

DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `brand`
--

INSERT INTO `brand` (`id`, `label`) VALUES
(1, 'Gibson'),
(2, 'Fender'),
(3, 'Marshall'),
(6, 'Boss'),
(8, 'Squier'),
(9, 'Ibanez'),
(10, 'Yamaha'),
(11, 'Dunlop'),
(12, 'Line 6'),
(13, 'Peevee'),
(14, 'Peevee');

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `street_name` varchar(255) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_mail_uindex` (`mail`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`id`, `mail`, `last_name`, `first_name`, `street_name`, `postal_code`, `city`) VALUES
(2, 'bob.bricaud@gmail.com', 'BRICAUD', 'Bob', '117, Rue du Chief', '11717', 'Gibson'),
(3, 'stephen.mistayan@gmail.com', 'Mistayan', 'Stephen', '42, Rue du Nerd', '42069', 'Kali'),
(4, 'gweltaz.garch@gmail.com', 'Bréard', 'Gweltaz', '2, Rue des Magnolias', '35000', 'Rennes'),
(5, 'orel@gmail.com', 'Sullfu', 'Orel', '9, Rue Développée', '35000', 'Beauregard'),
(6, 'anouk.helio@gmail.com', 'Heliotrope', 'Anouk', '9, Rue Développée', '35000', 'Beauregard'),
(7, 'alan.ileqziah@gmail.com', 'Ile', 'Alan', '2, Rue du Sel', '45000', 'Springfield'),
(8, 'mickael.ceredin@gmail.com', 'Ceredin', 'Mickaël', '34, Avenue De Gaulle', '47470', 'Python-Ville'),
(9, 'hadrien.fortin@gmail.com', 'Fortin', 'Hadrien', '2, Rue de l\'EPSI', '45000', 'Nantes'),
(14, 'fortin.perle@gmail.com', 'Fortin', 'Perle', '42, Rue des Roses', '45000', 'PARIS'),
(20, 'JoannaArsenault@jourrapide.com', 'Arsenault', 'Joanna', '78, Rue des Lilas', '98700', 'Aries'),
(21, 'RussellDaigle@armyspy.com', 'DAIGLE', 'Russell', '40, Avenue Fréquentée', '50940', 'Elliv'),
(22, 'AmedeeLabrecque@rhyta.com', 'Labrecque', 'Amedee', '1, Impasse sans sortie', '43900', 'Leliv'),
(23, 'MalagigiDastous@dayrep.com', 'Dastous', 'Malagigi', '9, Boulevard Draveluob', '39200', 'Amiens'),
(24, 'AimeDastous@rhyta.com', 'Dastous', 'Aime', '39, Rue de la Rue', '88800', 'Huis-ville'),
(25, 'MayhewEdouard@teleworm.us', 'Mayhew', 'Edouard', '114, Avenue des Champs-lysées', '99999', 'Parisse'),
(26, 'KermanDoucet@rhyta.com', 'Doucet', 'Kerman', '4, Place déplacée', '95400', 'Bourgeon'),
(27, 'OraneBouvier@dayrep.com', 'Bouvier', 'Orane', '84, Garage Street', '424242', 'Karentedeu'),
(28, 'DonaldWHutchens@rhyta.com', 'Hutchen', 'Donald W', '98 PARKSIDE', '33190', 'Menophis'),
(29, 'CarolFAnderson@jourrapide.com', 'Anderson', 'Carol', '4, Angel Avenue', '955555', 'Nede'),
(30, 'ElviraEKardos@jourrapide.com', 'Kardos', 'Elvira', '33, Third Street', '330333', 'Trantroy'),
(31, 'ThomasSEnnis@teleworm.us', 'Ennis', 'Thomas', '2, Boulevard Trogren', '505050', 'Pacégren'),
(32, 'DorothyLPercy@armyspy.com', 'Percy', 'Dorothy', '56, Rue Travers', '94500', 'WayWay');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `category` int(11) NOT NULL DEFAULT 4,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `brand`, `model`, `price`, `category`) VALUES
(1, 1, 'SG', 1250, 1),
(2, 2, 'Telecaster', 1200, 1),
(3, 2, 'Stratocaster', 1100, 1),
(4, 4, 'Looper RC-1', 150, 2),
(5, 3, 'JCM2203', 1000, 3),
(21, 3, 'WahWah', 120, 2),
(7, 1, 'Explorer', 3000, 1),
(16, 1, 'FlyingV', 4500, 1),
(20, 6, 'Overdrive', 80, 2),
(26, 12, 'M5 Stompbox', 128, 2),
(27, 12, 'Spider V', 200, 3);

-- --------------------------------------------------------

--
-- Structure de la table `productorder`
--

DROP TABLE IF EXISTS `productorder`;
CREATE TABLE IF NOT EXISTS `productorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `total_price` double DEFAULT NULL,
  `closed` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `productorder`
--

INSERT INTO `productorder` (`id`, `customer`, `date`, `total_price`, `closed`) VALUES
(87, 2, '2022-06-12 22:29:09', 2650, 1),
(88, 4, '2022-06-12 22:34:16', 3550, 1),
(89, 3, '2022-06-12 23:40:05', 3100, 1),
(90, 4, '2022-06-13 00:54:27', 4950, 1),
(91, 2, '2022-06-13 09:17:46', 3750, 1),
(179, 2, '2022-06-19 09:46:04', 2500, 0),
(178, 3, '2022-06-19 09:45:20', 4400, 0),
(177, 2, '2022-06-19 09:44:52', 3750, 0),
(95, 2, '2022-06-13 15:15:33', 6000, 1),
(180, 2, '2022-06-19 09:48:11', 3600, 0),
(176, 2, '2022-06-19 09:41:54', 2640, 0),
(175, 26, '2022-06-19 08:51:21', 17500, 0),
(99, 2, '2022-06-14 20:41:15', 4700, 1),
(100, 2, '2022-06-14 20:41:15', 4700, 1),
(101, 3, '2022-06-14 20:44:42', 4000, 1),
(102, 3, '2022-06-14 20:45:10', 3300, 1),
(103, 2, '2022-06-14 20:46:41', 5500, 1),
(104, 3, '2022-06-14 20:48:10', 3300, 1),
(105, 3, '2022-06-14 20:48:30', 150, 1),
(106, 3, '2022-06-14 20:48:56', 3600, 0),
(107, 5, '2022-06-14 20:48:59', 1150, 0),
(108, 2, '2022-06-14 21:15:08', 6250, 1),
(109, 2, '2022-06-14 21:16:15', 3300, 0),
(110, 3, '2022-06-14 21:16:18', 3300, 1),
(111, 5, '2022-06-14 21:16:21', 3600, 1),
(112, 2, '2022-06-14 21:19:16', 2250, 1),
(113, 4, '2022-06-14 21:19:19', 2250, 0),
(114, 8, '2022-06-14 21:19:22', 2250, 0),
(115, 2, '2022-06-14 21:23:13', 5000, 1),
(116, 2, '2022-06-14 21:23:16', 4400, 0),
(117, 4, '2022-06-14 21:23:18', 1000, 0),
(173, 2, '2022-06-19 08:38:15', 12150, 0),
(119, 4, '2022-06-14 21:23:24', 3000, 1),
(120, 4, '2022-06-14 21:23:27', 4000, 1),
(174, 2, '2022-06-19 08:50:23', 11250, 0),
(122, 2, '2022-06-14 21:44:38', 2400, 0),
(197, 2, '2022-06-19 15:45:29', 15240, 0),
(191, 2, '2022-06-19 12:23:58', 7200, 0),
(192, 2, '2022-06-19 12:24:59', 7200, 0),
(164, 3, '2022-06-15 13:41:14', 3600, 0),
(163, 6, '2022-06-15 02:26:00', 3300, 0),
(198, 2, '2022-06-19 16:52:11', 8370, 0),
(194, 29, '2022-06-19 14:20:46', 8720, 0),
(195, 2, '2022-06-19 14:21:43', 6850, 0),
(196, 21, '2022-06-19 14:25:37', 11700, 0),
(171, 2, '2022-06-19 06:40:10', 6250, 0),
(190, 31, '2022-06-19 10:58:45', 7200, 0),
(162, 2, '2022-06-15 01:19:41', 3550, 0),
(193, 29, '2022-06-19 14:18:24', 9160, 0),
(186, 2, '2022-06-19 10:44:25', 2400, 0),
(187, 2, '2022-06-19 10:52:52', 2520, 0),
(188, 2, '2022-06-19 10:54:10', 7500, 0),
(189, 5, '2022-06-19 10:54:54', 3600, 0),
(182, 2, '2022-06-19 10:11:24', 0, 0),
(183, 32, '2022-06-19 10:20:23', 3750, 0),
(184, 2, '2022-06-19 10:41:28', 1250, 0),
(185, 5, '2022-06-19 10:43:30', 3600, 0),
(168, 2, '2022-06-18 15:01:03', 10800, 1),
(169, 2, '2022-06-19 01:48:52', 9000, 1),
(170, 2, '2022-06-19 01:49:55', 13500, 0),
(181, 2, '2022-06-19 09:51:44', 3550, 0),
(172, 2, '2022-06-19 06:40:33', 360, 1),
(166, 2, '2022-06-16 22:24:48', 3250, 0),
(167, 2, '2022-06-18 13:29:58', 3700, 0),
(199, 2, '2022-06-19 17:09:48', 3750, 0);

-- --------------------------------------------------------

--
-- Structure de la table `productorder_details`
--

DROP TABLE IF EXISTS `productorder_details`;
CREATE TABLE IF NOT EXISTS `productorder_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=492 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `productorder_details`
--

INSERT INTO `productorder_details` (`id`, `id_order`, `id_product`, `quantity`) VALUES
(61, 90, 1, 1),
(64, 91, 1, 1),
(74, 93, 4, 1),
(62, 91, 1, 1),
(65, 92, 2, 1),
(67, 92, 2, 1),
(63, 91, 1, 1),
(66, 92, 2, 1),
(54, 88, 3, 1),
(57, 89, 3, 1),
(73, 93, 1, 1),
(70, 92, 2, 1),
(75, 93, 4, 1),
(72, 92, 2, 1),
(69, 92, 2, 1),
(49, 87, 1, 1),
(71, 92, 2, 1),
(68, 92, 2, 1),
(56, 89, 5, 1),
(53, 88, 2, 1),
(58, 90, 2, 1),
(55, 89, 5, 1),
(52, 88, 1, 1),
(51, 87, 4, 1),
(50, 87, 1, 1),
(59, 90, 1, 1),
(76, 93, 4, 1),
(60, 90, 1, 1),
(77, 93, 4, 1),
(78, 94, 2, 1),
(79, 94, 2, 1),
(80, 94, 2, 1),
(81, 95, 2, 1),
(82, 95, 2, 1),
(83, 95, 2, 1),
(84, 95, 2, 1),
(85, 95, 2, 1),
(86, 96, 4, 1),
(87, 96, 4, 1),
(88, 96, 4, 1),
(89, 97, 5, 1),
(90, 97, 5, 1),
(91, 97, 5, 1),
(92, 97, 5, 1),
(93, 97, 5, 1),
(94, 98, 1, 1),
(95, 98, 1, 1),
(96, 98, 1, 1),
(97, 98, 1, 1),
(98, 98, 1, 1),
(99, 98, 4, 1),
(100, 98, 4, 1),
(101, 99, 1, 1),
(102, 100, 1, 1),
(103, 99, 2, 1),
(104, 100, 2, 1),
(105, 99, 3, 1),
(106, 100, 3, 1),
(107, 99, 5, 1),
(108, 100, 5, 1),
(109, 99, 4, 1),
(110, 100, 4, 1),
(111, 101, 5, 1),
(112, 101, 5, 1),
(113, 101, 5, 1),
(114, 101, 5, 1),
(115, 102, 3, 1),
(116, 102, 3, 1),
(117, 102, 3, 1),
(118, 103, 3, 1),
(119, 103, 3, 1),
(120, 103, 3, 1),
(121, 103, 3, 1),
(122, 103, 3, 1),
(123, 104, 2, 1),
(124, 104, 3, 1),
(125, 104, 5, 1),
(126, 105, 4, 1),
(127, 106, 2, 1),
(128, 106, 2, 1),
(129, 106, 2, 1),
(130, 107, 4, 1),
(131, 107, 5, 1),
(132, 108, 1, 1),
(133, 108, 1, 1),
(134, 108, 1, 1),
(135, 108, 1, 1),
(136, 108, 1, 1),
(137, 109, 2, 1),
(138, 109, 3, 1),
(139, 109, 5, 1),
(140, 110, 2, 1),
(141, 110, 3, 1),
(142, 110, 5, 1),
(143, 111, 2, 1),
(144, 111, 2, 1),
(145, 111, 2, 1),
(146, 112, 3, 1),
(147, 112, 5, 1),
(148, 112, 4, 1),
(149, 113, 3, 1),
(150, 113, 5, 1),
(151, 113, 4, 1),
(152, 114, 4, 1),
(153, 114, 5, 1),
(154, 114, 3, 1),
(155, 115, 5, 1),
(156, 115, 5, 1),
(157, 115, 5, 1),
(158, 115, 5, 1),
(159, 115, 5, 1),
(160, 116, 3, 1),
(161, 116, 3, 1),
(162, 116, 3, 1),
(163, 116, 3, 1),
(164, 117, 5, 1),
(165, 119, 5, 1),
(166, 119, 5, 1),
(167, 119, 5, 1),
(168, 120, 5, 1),
(169, 120, 5, 1),
(170, 120, 5, 1),
(171, 120, 5, 1),
(172, 122, 2, 1),
(173, 122, 2, 1),
(417, 183, 1, 1),
(416, 183, 1, 1),
(423, 186, 2, 1),
(420, 185, 2, 1),
(424, 186, 2, 1),
(422, 185, 2, 1),
(419, 184, 1, 1),
(431, 188, 1, 1),
(421, 185, 2, 1),
(418, 183, 1, 1),
(430, 188, 1, 1),
(427, 187, 21, 1),
(432, 188, 1, 1),
(429, 188, 1, 1),
(426, 187, 2, 1),
(390, 176, 21, 1),
(428, 188, 1, 1),
(425, 187, 2, 1),
(389, 176, 21, 1),
(386, 176, 21, 1),
(391, 176, 21, 1),
(388, 176, 21, 1),
(385, 176, 21, 1),
(398, 176, 21, 1),
(387, 176, 21, 1),
(384, 176, 21, 1),
(397, 176, 21, 1),
(394, 176, 21, 1),
(399, 176, 21, 1),
(396, 176, 21, 1),
(393, 176, 21, 1),
(406, 178, 3, 1),
(395, 176, 21, 1),
(392, 176, 21, 1),
(405, 178, 3, 1),
(402, 177, 1, 1),
(407, 178, 3, 1),
(404, 178, 3, 1),
(401, 177, 1, 1),
(414, 181, 2, 1),
(403, 177, 1, 1),
(400, 176, 21, 1),
(413, 181, 1, 1),
(410, 180, 2, 1),
(415, 181, 3, 1),
(412, 180, 2, 1),
(409, 179, 1, 1),
(358, 174, 1, 1),
(411, 180, 2, 1),
(408, 179, 1, 1),
(357, 174, 1, 1),
(354, 173, 1, 1),
(359, 174, 1, 1),
(356, 174, 1, 1),
(353, 173, 1, 1),
(366, 175, 1, 1),
(355, 173, 1, 1),
(352, 173, 1, 1),
(365, 175, 1, 1),
(362, 174, 1, 1),
(367, 175, 1, 1),
(364, 174, 1, 1),
(361, 174, 1, 1),
(374, 175, 1, 1),
(363, 174, 1, 1),
(360, 174, 1, 1),
(373, 175, 1, 1),
(370, 175, 1, 1),
(375, 175, 1, 1),
(372, 175, 1, 1),
(369, 175, 1, 1),
(382, 176, 21, 1),
(371, 175, 1, 1),
(368, 175, 1, 1),
(381, 176, 21, 1),
(378, 175, 1, 1),
(383, 176, 21, 1),
(380, 176, 21, 1),
(377, 175, 1, 1),
(347, 173, 2, 1),
(379, 176, 21, 1),
(376, 175, 1, 1),
(439, 190, 5, 1),
(436, 189, 2, 1),
(341, 171, 1, 1),
(438, 190, 5, 1),
(435, 189, 2, 1),
(349, 173, 2, 1),
(437, 190, 2, 1),
(434, 189, 2, 1),
(350, 173, 2, 1),
(345, 172, 21, 1),
(314, 163, 3, 1),
(351, 173, 2, 1),
(346, 173, 1, 1),
(321, 167, 1, 1),
(344, 172, 21, 1),
(340, 171, 1, 1),
(332, 168, 2, 1),
(317, 164, 2, 1),
(322, 167, 1, 1),
(348, 173, 3, 1),
(316, 164, 2, 1),
(329, 168, 2, 1),
(318, 166, 1, 1),
(315, 164, 2, 1),
(328, 168, 2, 1),
(325, 168, 2, 1),
(330, 168, 2, 1),
(327, 168, 2, 1),
(324, 168, 2, 1),
(334, 169, 16, 1),
(326, 168, 2, 1),
(323, 167, 2, 1),
(335, 170, 16, 1),
(342, 171, 1, 1),
(339, 171, 1, 1),
(336, 170, 16, 1),
(343, 172, 21, 1),
(312, 163, 3, 1),
(337, 170, 16, 1),
(433, 188, 1, 1),
(311, 162, 3, 1),
(338, 171, 1, 1),
(313, 163, 3, 1),
(310, 162, 2, 1),
(333, 169, 16, 1),
(320, 166, 5, 1),
(309, 162, 1, 1),
(331, 168, 2, 1),
(319, 166, 5, 1),
(440, 190, 5, 1),
(441, 190, 5, 1),
(442, 190, 5, 1),
(443, 190, 5, 1),
(444, 191, 2, 1),
(445, 191, 2, 1),
(446, 191, 2, 1),
(447, 191, 2, 1),
(448, 191, 2, 1),
(449, 191, 2, 1),
(450, 192, 2, 1),
(451, 192, 2, 1),
(452, 192, 2, 1),
(453, 192, 2, 1),
(454, 192, 2, 1),
(455, 192, 2, 1),
(456, 193, 2, 1),
(457, 193, 2, 1),
(458, 193, 3, 1),
(459, 193, 16, 1),
(460, 193, 20, 1),
(461, 193, 20, 1),
(462, 193, 5, 1),
(463, 194, 7, 1),
(464, 194, 16, 1),
(465, 194, 21, 1),
(466, 194, 3, 1),
(467, 195, 1, 1),
(468, 195, 3, 1),
(469, 195, 16, 1),
(470, 196, 7, 1),
(471, 196, 7, 1),
(472, 196, 16, 1),
(473, 196, 21, 1),
(474, 196, 20, 1),
(475, 196, 5, 1),
(476, 197, 7, 1),
(477, 197, 7, 1),
(478, 197, 16, 1),
(479, 197, 16, 1),
(480, 197, 21, 1),
(481, 197, 21, 1),
(482, 198, 1, 1),
(483, 198, 1, 1),
(484, 198, 1, 1),
(485, 198, 21, 1),
(486, 198, 16, 1),
(487, 199, 20, 1),
(488, 199, 21, 1),
(489, 199, 2, 1),
(490, 199, 1, 1),
(491, 199, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `product_category`
--

INSERT INTO `product_category` (`id`, `name`) VALUES
(1, 'Guitar'),
(2, 'Pedal'),
(3, 'Guitar Amps'),
(4, 'default');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_mail_uindex` (`mail`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `mail`, `password`, `role`) VALUES
(2, 'dsi@acme.com', '$2a$10$g9vlfrGBM8oFMClgcOgHhOwtOp7c/tsqeppOQ2UsvWKeGlRp296wq', 'ROLE_CIO'),
(3, 'stephen@mistayan', '$2a$10$EPXCldZDk7Au4blqAPjOrOssulU9eHn/eA2aHhpkcChK0VpqusLBO', 'ROLE_EMPLOYEE'),
(4, 'John.doe@gmail.com', '$2a$10$xNQlEkKpPStxH.MPKgpeJO4PeJtcBoK4cZ0/VdLS.UamlzCm7Ljva', 'ROLE_EMPLOYEE'),
(5, 'John.dog@gmail.com', '$2a$10$lDQtg17gmw5ODi/3bUWWtuuankRTUnNNn5t5rDV2blY3uollTRJka', 'ROLE_SALESMAN');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

var app = new Vue({
    el: '#app',
    data() {
        return {
            customers: [],
            update_customer: {},
            newCustomer: {},
            current_customer: null,
            create: false,
        }
    },
    mounted() {
        axios.get('/salesman/add_customer')
            .then(response => {
                this.customers = response.data.data;
            })
    },
    methods: {
        createCustomer() {
            axios.post('/salesman/add_customer', this.newCustomer)
                .then(response => {
                    if (response.data.success) {
                        this.newCustomer = {};
                        this.create = false;
                        axios.get('/salesman/add_customer')
                            .then(response => {
                                this.customers = response.data.data;
                            })
                    }
                })
        },
        updateCustomer(current_customer) {
            axios.put("/salesman/update_customer", this.current_customer)
                .then(response => {
                    if (response.data.success) {
                        this.current_customer = {};
                        window.location.replace("add_customer.html")
                    } else {
                        console.log("FAILED")
                    }
                })

        },
        setCurrentCustomer(selected_customer) {
            this.current_customer = selected_customer;
            let update_customer = selected_customer;


        }
    }
})

const app = new Vue({
    el: '#app',
    data() { //le modèle de données

        return {
            products: [],
            brands: [],
            create: false,
            update: false,
            newProduct: {},
            changeProduct: {},
            newBrand: '',
            category_list: []
        }
    },
    mounted() { // Ce qui est affiché au chargement de la page
        axios.get("/products")
            .then(response => {
                this.products = response.data.data;
            });
         // this.brands = await (fetch("http://127.0.0.117:1017/brands"))
        axios.get("/brands")
            .then(response => {
                this.brands = response.data
                // for (const [key, value] of Object.entries(this.brands)) {
                //     console.log(`${key} (((((: ${value}`);
                // }
            });
        axios.get("/category")
            .then(response => {
                this.category_list = response.data
                // for (const [key, value] of Object.entries(this.brands)) {
                //     console.log(`${key} (((((: ${value}`);
                // }
            })
    },

    methods: {
        showById(product) {
            axios.get("/product", product)

        },
        addProduct() {
            axios.post("/products", this.newProduct)
                .then(response => {
                    if (response.data.success) {
                        this.newProduct = {};
                        this.create = false;
                        axios.get("/products")
                            .then(response => {
                                this.products = response.data.data;
                            })
                    }
                })
        },
        check_existing_brand(obj) {
            return (Object.keys(this.brands).filter(key=>this.brands[key].toUpperCase() === obj.toUpperCase()).length > 0)
            // return obj ? true : (Object.keys(this.brands).filter(key=>this.brands[key].toUpperCase() === obj.toUpperCase()).length > 0)
            // return Object.keys(this.brands).filter(key=>obj[key].toUpperCase() === this.newBrand.toUpperCase()).length

        },

        post_new_brand(){
            const requestOptions = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: this.newBrand

            };
            fetch('http://127.0.0.117:1017/brands', requestOptions)
                .then(async response => {
                    const data = await response.json();
                    if (data.success) {
                        // popup new brand created : Gibson
                    }
                })
                .catch(error => {
                    console.error('There was an error!', error);
                });

        },

        addBrand() {
            Object.keys(this.brands).filter(key=>this.brands[key].toUpperCase() === this.newBrand.toUpperCase()).length ? this.cant_post_new_brand() : this.post_new_brand()
        },
        cant_post_new_brand(){ console.log("nope")},
        deleteProduct(product) {
            axios.post("/delete_products", product)
                .then(response => {
                    if (response.data.success) {
                        axios.get("/products")
                            .then(response => {
                                this.products = response.data.data;
                            })
                    }
                })
        },
        updateProduct() {
            // this.changeProduct("id") = truc;
            // let productId = localStorage.getItem("product.id");
            axios.post("/update_products", this.changeProduct)
                .then(response => {
                    if (response.data.success) {
                        this.changeProduct = {};
                        this.update = false;
                        axios.get("/products")
                            .then(response => {
                                this.products = response.data.data;
                            })
                    }
                })
        }
    }
});
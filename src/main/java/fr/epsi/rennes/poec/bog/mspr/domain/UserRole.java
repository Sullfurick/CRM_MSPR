package fr.epsi.rennes.poec.bog.mspr.domain;

import org.springframework.security.core.GrantedAuthority;

/*UserRole class*/
public enum UserRole implements GrantedAuthority {
    /*Defines the roles inside the application*/
    ROLE_EMPLOYEE, ROLE_SALESMAN, ROLE_CIO;

    /*Used to get the role*/
    @Override
    public String getAuthority() {
        return this.name();
    }

}


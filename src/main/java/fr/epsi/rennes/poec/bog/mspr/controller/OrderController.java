package fr.epsi.rennes.poec.bog.mspr.controller;


import fr.epsi.rennes.poec.bog.mspr.domain.Order;
import fr.epsi.rennes.poec.bog.mspr.domain.Product;
import fr.epsi.rennes.poec.bog.mspr.domain.Response;
import fr.epsi.rennes.poec.bog.mspr.exception.BusinessException;
import fr.epsi.rennes.poec.bog.mspr.service.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {

    private static final Logger logger = LogManager.getLogger(String.valueOf(OrderController.class));


    @Autowired
    private OrderService orderService;

    @PostMapping("/order")
    public Response<Void> createOrder(@RequestBody Order order) throws BusinessException, SQLException {
        logger.debug("ORDER CONTROLLER POST");
        return orderService.postOrder(order.getCustomer(), order.getProductlist());
    }

    @GetMapping("/orders")
    public Response<List<Order>> getOrders() throws SQLException {
        logger.debug("ORDER CONTROLLER GET");

        List<Order> orders = orderService.getOrders();
        Response<List<Order>> response = new Response<>();

//        for (Order order : orders) {
//            logger.info(order.getCustomer() + order.getCustomer().getLast_name());
//        }
        response.setData(orders);

        return response;
    }

    @GetMapping("/category_best_seller")
    public Response<ArrayList> getCategory_best_seller() {
        logger.debug("ORDER CONTROLLER getCategory_best_seller");
        return orderService.getCategory_best_seller();
    }

    @GetMapping("/brand_best_seller")
    public Response<ArrayList> getBrand_best_seller() {
        logger.debug("ORDER CONTROLLER getBrand_best_seller");
        return orderService.getBrand_best_seller();
    };

    @GetMapping("/order_per_day")
    public Response<Void> getOrders_by_date() {
        logger.debug("ORDER CONTROLLER getOrders_by_date");
        return orderService.getOrders_by_date();
    };

//    @GetMapping("/sorted_orders")
//    public Response<List<Order>> getSortedOrders() throws SQLException {
//        logger.info("GET getSortedOrders CONTROLLER");
//
//        List<Order> orders = orderService.getSortedOrders();
//        Response<List<Order>> response = new Response<>();
//
//        for (Order order : orders) {
//            logger.info(order.getCustomer() + order.getCustomer().getLast_name());
//        }
//        response.setData(orders);
//
//        return response;
//    }

    @GetMapping("/products_orders")
    public Response<List<Product>> getProductOrder() throws SQLException {
        logger.debug("ORDER CONTROLLER getProductOrder");
        List<Product> products = orderService.getProductOrder();
        Response<List<Product>> response = new Response<>();
        response.setData(products);
        return response;
    }

    @PutMapping("/update_order")
    public Response updateOrder(@RequestBody Integer order_id) {
        logger.debug("ORDER CONTROLLER updateOrder");
        return orderService.updateOrder(order_id);
    }

    @GetMapping("/brands")
    public Map<Integer, String> getBrands() throws SQLException {
        logger.debug("ORDER CONTROLLER getBrands");
        return orderService.getBrands();
    };
    @PostMapping("/brands")
    public Response<Void> postBrands(@RequestBody String newBrand) throws SQLException {
        logger.debug("ORDER CONTROLLER postBrands");
        return orderService.postBrands(newBrand);
    };

    @GetMapping("/category")
    public List<String> getCategory() throws SQLException {
        logger.debug("ORDER CONTROLLER getCategory");
        return orderService.getCategory();
    };

    @GetMapping("/last_order")
    public Order getCustomer_last_order(@RequestParam int customer_id) throws SQLException {
        logger.debug("ORDER CONTROLLER getCustomer_last_order");
        return orderService.getCustomer_last_order(customer_id);
    };


}

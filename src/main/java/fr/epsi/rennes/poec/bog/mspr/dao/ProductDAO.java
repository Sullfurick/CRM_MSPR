package fr.epsi.rennes.poec.bog.mspr.dao;

import fr.epsi.rennes.poec.bog.mspr.domain.Product;
import fr.epsi.rennes.poec.bog.mspr.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductDAO {

    @Autowired
    private DataSource ds;
    private String sql_category;


    public List<Product> getAllProducts() throws SQLException {
        String sql = "SELECT product.id, brand.label, product.model, product.price FROM product INNER JOIN brand ON product.brand = brand.id";
        List<Product> products = new ArrayList<>();
        try (Connection conn = ds.getConnection();
        PreparedStatement stmt = conn.prepareStatement(sql)){


            // attention au nom des colonnes de la table ainsi qu'à la quantité
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                Product product = new Product();
                product.setId(rs.getInt("product.id"));
                product.setBrand(rs.getString("brand.label"));
                product.setModel(rs.getString("product.model"));
                product.setPrice(rs.getDouble("product.price"));
                products.add(product);

            }

        }catch (SQLException e){
            throw new TechnicalException(e);
        }
    return products;
    }

    public Product getProductById (int productId){
        String sql = "select * " +
                "from product " +
                "where product.id = ? ";

        try(Connection conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);){
            ps.setInt(1, productId);

            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                Product product = new Product();
                product.setId(rs.getInt("id"));
                product.setModel(rs.getString("model"));
                product.setBrand(rs.getString("brand"));
                product.setPrice(rs.getDouble("price"));

                return product;
            }
        }catch (SQLException e){
            throw new TechnicalException(e);
        }


        return null;
    }

    public int addProduct(String model, String brand, String category, double price) throws SQLException{

        String sql_brand = "SELECT id FROM brand WHERE UPPER(label) LIKE UPPER('" + brand + "');";
        int result_id_brand = 0;
        try ( Connection conn = ds.getConnection();
              PreparedStatement ps = conn.prepareStatement(sql_brand)) {
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                result_id_brand = (rs.getInt("id"));
            }
        }catch (SQLException e) {throw  new TechnicalException(e);}

        String sql_category = "SELECT id FROM product_category WHERE UPPER(name) LIKE UPPER('" + category + "');";
        int result_id_category = 0;
        try ( Connection conn = ds.getConnection();
              PreparedStatement ps = conn.prepareStatement(sql_category)) {
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                result_id_category = (rs.getInt("id"));
            }
        }catch (SQLException e) {throw  new TechnicalException(e);}

        String sql = "insert into product (model, brand, category, price) values (?, ?, ?,?)";
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
            ps.setString(1, model);
            ps.setInt(2, result_id_brand);
            ps.setInt(3, result_id_category);
            ps.setDouble(4, price);
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next()){
                return rs.getInt(1);
            }
            throw new TechnicalException(new SQLException("Error creating product"));
        }catch (SQLException e){
            throw new TechnicalException(e);
        }
    }

    public void deleteProduct(int id) throws  SQLException {
        String sql = "delete from product where id = ?";
        try (Connection conn = ds.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e){
            throw new TechnicalException(e);
        }
    }

    public void updateProduct(String model, String brand, double price, int id) throws SQLException{

        String sql_brand = "SELECT id FROM brand WHERE UPPER(label) LIKE UPPER('" + brand + "');";
        int result_id_brand = 0;
        try ( Connection conn = ds.getConnection();
              PreparedStatement ps = conn.prepareStatement(sql_brand)) {
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                result_id_brand = (rs.getInt("id"));
            }
        }catch (SQLException e) {throw  new TechnicalException(e);}

        String sql = "Update product set model = ?, brand = ?, price = ? where id = ?;";
        // sql = "Update product set model = ?, brand = brand.id, price = ? FROM product INNER JOIN brand ON brand.label = ? WHERE id = ?;";

        try (Connection conn = ds.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)){

            ps.setString(1, model);
            ps.setInt(2, result_id_brand);
            ps.setDouble(3, price);
            ps.setInt(4, id);
            //System.out.println(id);
            ps.executeUpdate();
        } catch (SQLException e){
            throw  new TechnicalException(e);
        }
    };
}

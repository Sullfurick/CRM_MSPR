package fr.epsi.rennes.poec.bog.mspr.service;

import fr.epsi.rennes.poec.bog.mspr.dao.CustomerDAO;
import fr.epsi.rennes.poec.bog.mspr.domain.Customer;
import fr.epsi.rennes.poec.bog.mspr.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/*CustomerService class*/
@Service
public class CustomerService {
    /*Connection to the database*/
    @Autowired
    private CustomerDAO customerDAO;

    /*Get getAllNameCustomer method from CustomerDAO*/
    public List<Customer> getAllNameCustomer() throws SQLException {
        return customerDAO.getAllNameCustomer();
    }

    /*Get getAllInfoCustomer method from CustomerDAO*/
    public List<Customer> getAllInfoCustomer() throws SQLException {
        return customerDAO.getAllInfoCustomer();
    }

    public Customer getCustomerById(Integer customerId) throws SQLException {
        return customerDAO.getCustomerById(customerId);
    }

    /*Get createCustomer method from CustomerDAO*/
    public void createCustomer(Customer customer) {
        customerDAO.createCustomer(customer.getMail(), customer.getLast_name(),
                customer.getFirst_name(), customer.getStreet_name(),
                customer.getPostal_code(), customer.getCity());
    }

    /*Get updateCustomer method from CustomerDAO*/
    public boolean updateCustomer(Customer update_customer) {
        try {
            return customerDAO.updateCustomer(update_customer);
        } catch (TechnicalException e) {
            return false;
        }
    }
}


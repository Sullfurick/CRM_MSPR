package fr.epsi.rennes.poec.bog.mspr.domain;

import java.util.Map;

/*Response class to communicate with the JS*/
public class Response<T> {

    private T data;
    private boolean success = true;

    private int status;

    private int new_id;

    private String error_message;

    public Order order;

    private Map<Integer, String> MapData;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNew_id() {
        return new_id;
    }

    public void setNew_id(int new_id) {
        this.new_id = new_id;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Map<Integer, String> getMapData() {
        return MapData;
    }

    public void setMapData(Map<Integer, String> mapData) {
        MapData = mapData;
    }
}

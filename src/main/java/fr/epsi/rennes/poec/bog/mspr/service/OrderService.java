package fr.epsi.rennes.poec.bog.mspr.service;

import fr.epsi.rennes.poec.bog.mspr.dao.OrderDAO;
import fr.epsi.rennes.poec.bog.mspr.domain.Customer;
import fr.epsi.rennes.poec.bog.mspr.domain.Order;
import fr.epsi.rennes.poec.bog.mspr.domain.Product;
import fr.epsi.rennes.poec.bog.mspr.domain.Response;
import fr.epsi.rennes.poec.bog.mspr.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class OrderService {

    private static final Logger logger = LogManager.getLogger(String.valueOf(OrderService.class));
    @Autowired
    private OrderDAO orderDao;

    public List<Order> getOrders() throws SQLException {
        logger.debug("ORDER SERVICE GET");
        Response<Long> response = new Response();
        try {
            return orderDao.getOrders();
        }
        catch (TechnicalException e) {
            response.setSuccess(false);
            logger.fatal("user/order route userService.userOrder failed :::> " + e);
            throw new RuntimeException("cant get orders");
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

    };

    public Response<Void> postOrder(Customer customer, ArrayList<Product> productList) throws SQLException {
        logger.debug("ORDER SERVICE POST");
        Double total_price = 0.0;
        for(Product product: productList) {
            total_price += product.getPrice();
        };
        return orderDao.postOrder(customer.getId(), productList, total_price);
    };

    public List<Product> getProductOrder() {
        logger.debug("ORDER SERVICE getProductOrder");
        Response<Long> response = new Response();
        try {
            return orderDao.getProductOrders();
        }
        catch (TechnicalException e) {
            response.setSuccess(false);
            logger.fatal("user/order route userService.userOrder failed :::> " + e);
            throw new RuntimeException("cant get product for this order");
        }
    };

    public Response<Order> updateOrder(int order_id) {
        logger.debug("ORDER SERVICE PUT");
        try {
            return orderDao.updateOrder(order_id);
        } catch (TechnicalException e) {logger.fatal(e); throw new RuntimeException("cant update order with id");}
    }

    public Response<ArrayList> getCategory_best_seller() {
        logger.debug("ORDER SERVICE getCategory_best_seller");
        try {
            return orderDao.getCategory_best_seller();
        } catch( TechnicalException e) {
            logger.fatal(e); throw new RuntimeException("cant get category best seller");
        }
    }

    public Response<ArrayList> getBrand_best_seller() {
        logger.debug("ORDER SERVICE getBrand_best_seller");
        try {
            return orderDao.getBrand_best_seller();
        } catch( TechnicalException e) {
            logger.fatal(e); throw new RuntimeException("cant get brand best seller");
        }
    }

    public Response<Void> getOrders_by_date() {
        logger.debug("ORDER SERVICE getOrders_by_date");

        try {
            return orderDao.getOrders_by_date();
        } catch( TechnicalException e) {
            logger.fatal(e); throw new RuntimeException("cant get orders by date");
        }
    };

    public Map<Integer, String> getBrands() throws SQLException {
        logger.debug("ORDER SERVICE getBrands");
//        Response< Map<Integer, String>> response = new Response();
        Map<Integer, String> brands;
        try {
            brands = orderDao.getBrands();
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

        return brands;

    };

    public Response<Void> postBrands(String newBrand) {
        logger.debug("ORDER SERVICE postBrands");
        return orderDao.postBrand(newBrand);
    }

    public List<String> getCategory() throws SQLException {
        logger.debug("ORDER SERVICE getCategory");
//        Response< Map<Integer, String>> response = new Response();
        List<String> category;
        try {
            category = orderDao.getCategory();

        } catch (SQLException e) {
            throw new TechnicalException(e);
        }
        return category;
    };

    public Order getCustomer_last_order(int customer_id) {
        logger.debug("ORDER SERVICE getCustomer_last_order");
        try {
            return orderDao.getCustomer_last_order(customer_id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };


}

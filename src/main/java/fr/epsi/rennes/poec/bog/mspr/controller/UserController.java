package fr.epsi.rennes.poec.bog.mspr.controller;

import fr.epsi.rennes.poec.bog.mspr.domain.User;
import fr.epsi.rennes.poec.bog.mspr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/*UserController class*/
@RestController
public class UserController {


    /*Link the UserService*/
    @Autowired
    private UserService userService;

    /*Post the addEmployee and return a response for the JS*/
    @PostMapping("/cio/add_employee")
    public void addEmployee(User user) {
        userService.addEmployee(user);
    }

    /*Post the addSalesman and return a response for the JS*/
    @PostMapping("/cio/add_salesman")
    public void addSalesman(User user) {
        userService.addSalesman(user);
    }
}
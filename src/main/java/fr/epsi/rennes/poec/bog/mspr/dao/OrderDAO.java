package fr.epsi.rennes.poec.bog.mspr.dao;

import fr.epsi.rennes.poec.bog.mspr.domain.Customer;
import fr.epsi.rennes.poec.bog.mspr.domain.Order;
import fr.epsi.rennes.poec.bog.mspr.domain.Product;
import fr.epsi.rennes.poec.bog.mspr.domain.Response;
import fr.epsi.rennes.poec.bog.mspr.exception.TechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OrderDAO {

    private static final Logger logger = LogManager.getLogger(String.valueOf(OrderDAO.class));

    private final DataSource data_source;

    @Autowired
    public OrderDAO(DataSource data_source) {
        this.data_source = data_source;
    }
    public DataSource getDs() {
        return this.data_source;
    }

/**
 * Select all orders, with a customer and a product list
 **/
    public List<Order> getOrders() throws SQLException {
        logger.debug("ORDER DAO GET");
        StringBuilder sql = new StringBuilder("SELECT productorder.id, productorder.total_price, productorder.date, productorder.closed, " +

                "customer.id, customer.last_name, customer.first_name, customer.street_name, customer.postal_code, customer.city, " +

                "GROUP_CONCAT( product.id, ':', brand.label, ':', product.model, ':', product.price) as productlist FROM productorder " +

                "LEFT JOIN productorder_details ON productorder_details.id_order = productorder.id " +

                "LEFT JOIN customer ON customer.id = productorder.customer " +

                "LEFT JOIN product ON product.id = productorder_details.id_product " +

                " LEFT JOIN brand ON brand.id = product.brand " +

                " GROUP BY productorder.id;");
        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(String.valueOf(sql))) {
            ResultSet rs = ps.executeQuery();
            List<Order> orders = new ArrayList<>();

            while (rs.next()) {
                Order current_order = new Order();

                current_order.setId(rs.getInt("productorder.id"));
                current_order.setDate(rs.getDate("productorder.date"));
                current_order.setTotal_price(rs.getDouble("productorder.total_price"));
                current_order.setClosed(rs.getBoolean("productorder.closed"));

                Customer current_customer = new Customer();
                current_customer.setId(rs.getInt("customer.id"));
                current_customer.setLast_name(rs.getString("customer.last_name"));
                current_customer.setFirst_name(rs.getString("customer.first_name"));
                current_customer.setStreet_name(rs.getString("customer.street_name"));
                current_customer.setPostal_code(rs.getString("customer.postal_code"));
                current_customer.setCity(rs.getString("customer.city"));

                current_order.setCustomer(current_customer);

                String list = rs.getString("productlist");

                ArrayList<Product> product_list = new ArrayList<>();

                if(list != null) { // to fix
                    for (String product : list.split(",")) {

                        Product current_product = new Product();
                        current_product.setId(Integer.parseInt(product.split(":")[0]));
                        current_product.setBrand(product.split(":")[1]);
                        current_product.setModel(product.split(":")[2]);
                        current_product.setPrice(Integer.parseInt(product.split(":")[3]));

                        product_list.add(current_product);

                    }
                    current_order.setProductlist(product_list);
                }
                orders.add(current_order);
            }
            return orders;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Insert into product_order, then insert into order_details with generated key
     **/
    @Transactional
    public Response<Void> postOrder(int customer_id, ArrayList<Product> product_list, Double total_price) throws SQLException {
        logger.debug("ORDER DAO POST");


        // calcul du prix total selon les prix réels de la db, et non ce que nous envoie notre application front
        double total_price_database = 0;
        try (Connection conn = data_source.getConnection()) {
            for (Product current_product : product_list) {
                total_price_database += get_order_price(current_product.getId(), conn);
            }

        }catch (SQLException e) { throw new TechnicalException(e);  }


        // Insert one order in product_order => insert for i in list w/ id = generated key
        String sql = "INSERT INTO `productorder` (`customer`, `date`, `total_price`) VALUES (?, NOW(), ?);";

        try (Connection conn = data_source.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, customer_id);
            ps.setDouble(2, total_price_database);

            // error handling if total_price_database != total_price
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                Response<Void> response = new Response();
                response.setStatus(201);
                response.setNew_id(rs.getInt(1));

                try {
                    for (Product current_product : product_list) {
                        insert_product_orderderdetails(rs.getInt(1), current_product.getId(), conn);
                    }
                }
                catch(Exception e) {
                    Response<Void> error_response = new Response();
                    error_response.setStatus(400); // bad request
                    String error_data = "cant create order";
                    error_response.setError_message(error_data);
                }
                return response;
            }
            throw new TechnicalException(new SQLException("Error creating order"));
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }
    }

/**
* Utils method to fill order_details table
**/
    public boolean insert_product_orderderdetails(int order_id, int product_id, Connection conn) throws SQLException {
        logger.debug("ORDER DAO insert_product_orderderdetails");
        String current_sql = "INSERT INTO `productorder_details` (`id_order`, `id_product`, `quantity`) VALUES ( ?, ?, 1)";
        PreparedStatement ps2 = conn.prepareStatement(current_sql);
        ps2.setInt(1, order_id);
        ps2.setInt(2, product_id);
        return ps2.executeUpdate() != 0;
    };

    /**
     * Utils method to fill order_details table
     **/
    public double get_order_price(int product_id, Connection conn) throws SQLException {
        logger.debug("ORDER DAO get_order_price");
        String slq_price = "SELECT product.price FROM product WHERE id = ?;";
        double current_price = 0;
        PreparedStatement ps2 = conn.prepareStatement(slq_price);
        ps2.setInt(1, product_id);
        ResultSet rs =ps2.executeQuery();
        while (rs.next()) {
            current_price = rs.getDouble("product.price");
        }
        return current_price;
    };



    /***
 *
 * Salesman access to all product to create order
 **/

    public List<Product> getProductOrders() {
        logger.debug("ORDER DAO GET PRODUCTS");

        String sql = "SELECT product.id, brand.label, product.model, product.price, product_category.name FROM product INNER JOIN brand ON product.brand = brand.id INNER JOIN product_category ON product_category.id = product.category";
        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            List<Product> products = new ArrayList<>();
            while (rs.next()) {
                Product current_product = new Product();
                current_product.setId(rs.getInt("product.id"));
                current_product.setBrand(rs.getString("brand.label"));
                current_product.setModel(rs.getString("product.model"));
                current_product.setPrice(rs.getInt("product.price"));
                current_product.setCategory(rs.getString("product_category.name"));

                products.add(current_product);
            }
            return products;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

    }

    /**
     * Update order from open to close
     * */
    public Response<Order> updateOrder(int order_id) {
        logger.debug("ORDER DAO PUT");

        String sql = "UPDATE productorder SET `closed` = 1 WHERE id = ? ;";

        try (Connection conn = data_source.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, order_id);

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                // static order_log.insert into  { ip: 127.0.0.1, action: create, what: order n rs.getInt(1), when: date() }
                Response<Order> response = new Response();

                String response_sql = "SELECT customer.last_name, customer.first_name, productorder.date FROM productorder " +
                        "INNER JOIN customer ON productorder.customer = customer.id "+
                        "WHERE productorder.id = ?;";
                try {

                    Connection response_conn = data_source.getConnection();
                    PreparedStatement response_ps = response_conn.prepareStatement(response_sql);
                    response_ps.setInt(1, order_id);

                    ResultSet response_rs = response_ps.executeQuery();

                    while (response_rs.next()) {
                        Order updated_current_order = new Order();
                        Customer current_customer = new Customer();
                        current_customer.setFirst_name(response_rs.getString("first_name"));
                        current_customer.setLast_name(response_rs.getString("last_name"));

                        updated_current_order.setCustomer(current_customer);
                        updated_current_order.setDate(response_rs.getDate("date"));

                        response.setOrder(updated_current_order);
                        response.setStatus(202);
                        return response;
                    }
                }
                catch(Exception e) {
                    //  Block of code to handle errors
                }


            }
            throw new TechnicalException(new SQLException("Error updating order"));
        } catch (SQLException e) {
            // response.setStatus(409)
            throw new TechnicalException(e);
        }

    }


    /**
     *
     *  We dont delete orders, but method is here anyway
     ***/
    public Response<Void> deleteOrder(int order_id) {
logger.debug("ORDER DAO DELETE");
        String sql = "DELETE FROM productorder WHERE id = ? ;";

        try (Connection conn = data_source.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, order_id);

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {

                logger.warn(rs.getInt(1));
                Response<Void> response = new Response();
                response.setStatus(202);
                response.setNew_id(rs.getInt(1));

                return response;
            }
            throw new TechnicalException(new SQLException("Error deleting order"));
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

    }


    /***
     *
     *Stats method to get the best selling category in all orders
     **/
    public Response<ArrayList> getCategory_best_seller() {
        logger.debug("ORDER DAO getCategory_best_seller");

        Response response = new Response();

        String sql = "SELECT (product_category.name) as cat, COUNT(product_category.id) as occurrence FROM productorder\n" +
                "INNER JOIN productorder_details ON productorder_details.id_order = productorder.id\n" +
                "INNER JOIN product ON product.id = productorder_details.id_product\n" +
                "INNER JOIN product_category ON product_category.id = product.category\n" +
                "GROUP BY product_category.name";

        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();

            Map<String, String> map = new HashMap<>();

            while (rs.next()) {
                map.put(rs.getString("cat"), rs.getString("occurrence"));
                response.setData(map);
            }
            return response;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }
    }


    /***
     *
     *Stats method to get the best selling brand in all orders
     **/
    public Response<ArrayList> getBrand_best_seller() {
        logger.debug("ORDER DAO getBrand_best_seller");

        Response response = new Response();

        String sql = "SELECT (brand.label) as brand, COUNT(brand.id) as occurrence FROM productorder\n" +
                "INNER JOIN productorder_details ON productorder_details.id_order = productorder.id\n" +
                "INNER JOIN product ON product.id = productorder_details.id_product\n" +
                "INNER JOIN brand ON brand.id = product.brand\n" +
                "GROUP BY brand.label";

        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            ArrayList<String> result = new ArrayList<>();

            Map<String, String> map = new HashMap<>();

            while (rs.next()) {
                map.put(rs.getString("brand"), rs.getString("occurrence"));
//                System.out.println(rs.getString("cat"));
//                System.out.println(rs.getString("occurrence"));
//                result.add(String.valueOf(new String[]{rs.getString("cat"), rs.getString("cat")}));
//                System.out.println(result);
                response.setData(map);
            }
            return response;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }
    }

    /***
     *
     *Stats method to get all orders by date
     **/
    public Response<Void> getOrders_by_date() {
        logger.debug("ORDER DAO getOrders_by_date");

        Response response = new Response();

        String sql = "SELECT hub.maindate as maindate, hub.number as number, hub.price as price from  (SELECT count(productorder.id) as number, sum(productorder.total_price) as price, productorder.date as maindate FROM productorder GROUP BY day(productorder.date) ASC \n" +
                "\n" +
                ") as hub";
        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();

            Map<String, String> map = new HashMap<>();

            while (rs.next()) {

                map.put(rs.getString("maindate"), rs.getString("number") + "?" + rs.getString("price"));
                response.setData(map);
            }
            return response;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

    };

    /***
     *
     * Fixing product CRUD, get all brands
     **/
    public Map<Integer, String> getBrands() throws SQLException {
        logger.info("ORDER DAO getBrands");
        String sql = "SELECT brand.id, brand.label FROM brand";
        Map<Integer, String> brands = new HashMap<Integer, String>();
        try (Connection conn = data_source.getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)){
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                brands.put(rs.getInt("brand.id"), rs.getString("brand.label"));
            }

        }catch (SQLException e){
            throw new TechnicalException(e);
        }
        return brands;
    }


    /***
     *
     * Fixing product CRUD, post brand
     **/
    public Response<Void> postBrand(String newBrand) {
        logger.info("ORDER DAO postBrand");
        String sql = "INSERT INTO brand (label) VALUES  ('"+ newBrand + "');";
        try (Connection conn = data_source.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            Response response = new Response();
            response.setSuccess(false);
            if(rs.next()){
                response.setNew_id(rs.getInt(1));
                response.setSuccess(true);
            };
            return response;
        }catch (SQLException e){
            throw new TechnicalException(e);
        }
    }

    /***
     *
     * Fixing product CRUD, get category
     **/
    public List<String> getCategory() throws SQLException {
        logger.info("ORDER DAO getCategory");
        String sql = "SELECT name FROM product_category";
        List<String> category = new ArrayList<>();
        try (Connection conn = data_source.getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)){
            ResultSet rs = stmt.executeQuery();
            while (rs.next()){
                category.add(rs.getString("name"));
            }

        }catch (SQLException e){
            throw new TechnicalException(e);
        }
        return category;
    }

    public Order getCustomer_last_order(int customer_id) throws SQLException {

        /**
         * SELECT c.last_name, hub.date from customer c
         * INNER JOIN (
         * SELECT productorder.customer as cust, productorder.date as date from productorder GROUP BY productorder.customer) as hub
         * ON c.id = hub.cust
         * GROUP BY c.id
         **/

        StringBuilder sql = new StringBuilder(
                "SELECT productorder.id, productorder.total_price, productorder.date, productorder.closed, GROUP_CONCAT( product.id, ':', brand.label, ':', product.model, ':', product.price) " +
                "as productlist FROM productorder "+
                "LEFT JOIN productorder_details ON productorder_details.id_order = productorder.id LEFT JOIN customer ON customer.id = productorder.customer "+
                "LEFT JOIN product ON product.id = productorder_details.id_product " +
                "LEFT JOIN brand ON brand.id = product.brand WHERE customer.id = ? GROUP BY productorder.id ORDER BY productorder.date DESC LIMIT 1;");

        Order order = new Order();
        try (
                Connection conn = data_source.getConnection();
                PreparedStatement ps = conn.prepareStatement(String.valueOf(sql))) {

            ps.setInt(1, customer_id);
            ResultSet rs = ps.executeQuery();
            Order current_order = new Order();

            while (rs.next()) {
                current_order.setDate(rs.getDate("productorder.date"));
                current_order.setClosed(rs.getBoolean("productorder.closed"));
                current_order.setTotal_price(rs.getDouble("productorder.total_price"));
                String list = rs.getString("productlist");
                ArrayList<Product> product_list = new ArrayList<>();
                if (list != null) { // to fix
                    for (String product : list.split(",")) {
                        Product current_product = new Product();
                        current_product.setId(Integer.parseInt(product.split(":")[0]));
                        current_product.setBrand(product.split(":")[1]);
                        current_product.setModel(product.split(":")[2]);
                        current_product.setPrice(Integer.parseInt(product.split(":")[3]));

                        product_list.add(current_product);

                    }
                    current_order.setProductlist(product_list);
                    return current_order;
                }
            }
            return current_order;
        } catch (SQLException e) {
            throw new TechnicalException(e);
        }

    }
}


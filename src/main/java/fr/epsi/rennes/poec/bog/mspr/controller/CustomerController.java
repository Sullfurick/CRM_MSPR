package fr.epsi.rennes.poec.bog.mspr.controller;

import fr.epsi.rennes.poec.bog.mspr.domain.Customer;
import fr.epsi.rennes.poec.bog.mspr.domain.Response;
import fr.epsi.rennes.poec.bog.mspr.exception.BusinessException;
import fr.epsi.rennes.poec.bog.mspr.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/*CustomerController class*/
@RestController
public class CustomerController {

    /*Link the CustomerService*/
    @Autowired
    private CustomerService customerService;

    /*Post the createCustomer and return a response for the JS*/
    @PostMapping("/salesman/add_customer")
    public Response<Void> createCustomer(@RequestBody Customer customer)
            throws BusinessException {
        customerService.createCustomer(customer);
        return new Response<>();
    }

    /*Get the getAllInfoCustomer and return a response for the JS*/
    @GetMapping("/salesman/add_customer")
    public Response<List<Customer>> getAllInfoCustomer()
            throws SQLException {
        List<Customer> customers = customerService.getAllInfoCustomer();

        Response<List<Customer>> response = new Response<>();
        response.setData(customers);

        return response;
    }
    @GetMapping("/employee/customers")
    public Response<List<Customer>> getAllCustomer()
            throws SQLException {
        List<Customer> customers = customerService.getAllInfoCustomer();

        Response<List<Customer>> response = new Response<>();
        response.setData(customers);

        return response;
    }

    /*Put the updateCustomer and return a response for the JS*/
    @PutMapping("/salesman/update_customer")
    public Response<Customer> updateCustomer(@RequestBody Customer update_customer) {
        Response<Customer> response = new Response<>();
        response.setSuccess(customerService.updateCustomer(update_customer));
        response.setData(update_customer);
        return response;
    }
}

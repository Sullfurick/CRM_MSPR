package fr.epsi.rennes.poec.bog.mspr.service;

import fr.epsi.rennes.poec.bog.mspr.dao.UserDAO;
import fr.epsi.rennes.poec.bog.mspr.domain.User;
import fr.epsi.rennes.poec.bog.mspr.domain.UserRole;
import fr.epsi.rennes.poec.bog.mspr.exception.TechnicalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/*UserService class*/
@Service
public class UserService implements UserDetailsService {

    /*Initiate the LogManager*/
    private static final Logger logger = LogManager.getLogger(UserService.class);
    /*Link the UserDAO*/
    @Autowired
    private UserDAO userDAO;

    /*Method to get a User by Username*/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            UserDetails user = userDAO.getUserByMail(username);
            if (user == null) {
                throw new UsernameNotFoundException("User not found : " + username);
            }
            return user;
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }

    /*Method to add an Employee to the database*/
    public void addEmployee(User user) {
        try {
            userDAO.setRole(UserRole.ROLE_EMPLOYEE.name());
            userDAO.addEmployee(user);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }

    /*Method to add a Salesman to the database*/
    public void addSalesman(User user) {
        try {
            userDAO.setRole(UserRole.ROLE_SALESMAN.name());
            userDAO.addSalesman(user);
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
            throw new TechnicalException(e);
        }
    }
}

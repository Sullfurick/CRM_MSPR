# POEC CYBERSECURITE 2022 - EPSI Rennes

## Mise en situation professionnelle reconstituée

# Membres :
- Olivier Bricaud https://gitlab.com/Bob-117/
- Aurélien Gouriou https://gitlab.com/Sullfurick
- Gweltaz Bréard https://gitlab.com/Garcharoth

# Fonctionnalités :
- Ajouter, Modifier, Lister, Supprimer des produits
- Ajouter, Modifier, Lister des clients
- Ajouter, Modifier, Lister des commandes
- Ajouter, Modifier des utilisateurs
- Visualiser des statistiques
- Gestion de rôles (Employee, Salesman, CIO)

# Technologies/langages utilisés
- Java
- HTML/CSS
- JavaScript
- Vue.JS
- Spring Boot
- Spring Security
- Bootstrap

## Un fichier SQL est disponible, à l'import dans phpMyAdmin, il créé une base de données fonctionnelle avec l'application (attention à bien remplir le fichier application.properties avec notamment les credentials du compte qu'utilisera l'application pour se connecter)

